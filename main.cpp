#include <iostream>
#include <string>

using namespace std;

int main(int argv, char* argv[])
{
  long magic_shift;
  float half;
  float acc;
  float real_value = 25;
  string target_value;

  // Check initial values
  if(argv < 2) {
    cout << "Please provide a number to parse as the first parameter." << endl;

    return 1;
  }

  // Convert value
  target_value = argv[1];
  real_value = stof(target_value);

  // Inverse the square of the value with two iterations
  half = 0.5f*real_value;
  acc = real_value;
  magic_shift = *(long*)&acc;
  magic_shift = 0x5f3759df - (magic_shift >> 1);
  acc = *(float*)&magic_shift;
  acc = acc*(threehalfs - (half*acc*acc)); // Repeat as necessary

  // Dump value
  cout << "Inverse square of " << real_value << " is " << acc << " or " << acc*(threehalfs - (half*acc*acc)) << endl;
  
  return 0;
}
